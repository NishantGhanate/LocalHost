package com.example.nishant.localhost;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editText1,editText2;
    Button buttonSend,buttonLogin;
    String a,b;
    //JSONParser jsonParser = new JSONParser();
    Background background ;
    String method;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);

        buttonSend = findViewById(R.id.buttonSignup);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = editText1.getText().toString();
                b = editText2.getText().toString();
                method ="Sign up";
                background = new Background(MainActivity.this);
                background.execute(method,a,b);
                a=b=method=null;
                editText1.setText("");
                editText2.setText("");

            }
        });

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = editText1.getText().toString();
                b = editText2.getText().toString();
                method ="Sign in";
                background = new Background(MainActivity.this);
                background.execute(method,a,b);
                a=b=method=null;
                editText1.setText("");
                editText2.setText("");
            }
        });






    }
}
